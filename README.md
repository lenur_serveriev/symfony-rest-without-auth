Symfony REST api without authentication 
=======================================

### The following bundles are used :
- https://github.com/FriendsOfSymfony/FOSRestBundle
- https://github.com/FriendsOfSymfony/FOSUserBundle
- https://github.com/nelmio/alice

### Using the API

Creating a new user (`POST`)

```bash
$ curl -i -H "Content-Type: application/json" -X POST -d '{"username" : "Test User1", "email" : "test1@mail.ru", "password" : "qwe"}' http://127.0.0.1:8000/users/create
```
Get all users (`GET`)

```bash
$ curl http://127.0.0.1:8000/users
```

Get one user (`GET`)

```bash
$ curl http://127.0.0.1:8000/users/1/show
```

Updating (`PUT`)

```bash
$ curl -i -H "Content-Type: application/json" -X PUT -d '{"username" : "Test 2222"}' http://127.0.0.1:8000/users/1/update
```

Delete (`DELETE`)

```bash
$ curl -X DELETE  http://127.0.0.1:8000/users/1/delete
```